# SIP3 Captain CE #

**SIP3 Captain** is a capturing agent and data adapter responsible for handling and encapsulating different protocols and sending data to **SIP3 Salto**.

## Intro

README will be updated soon. Meanwhile you can visit our [website](https://sip3.io/features) to get more details.

## Support

If you have a question about SIP3, just leave us a message in [Gitter](https://try.count.ly/at/6c2b2cf55c9e42f7835e8df7d990dfdfcdd4a5db), or send us an [email](mailto:support@sip3.io). We will be happy to help you.   